from django.urls import path, include
from rest_framework.documentation import include_docs_urls
from rest_framework import routers

from apps.views import *


router = routers.DefaultRouter()

router.register(r'users', UserViewSet, 'users')
router.register(r'videos', VideoViewSet, 'videos')

urlpatterns = [
    path('', include(router.urls)),
    path('login/', login),
    path('docs/', include_docs_urls(title='API Documentation'))
]


