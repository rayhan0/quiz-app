from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from django.views.decorators.csrf import csrf_exempt
from rest_framework.permissions import AllowAny
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from rest_framework.response import Response
from .models import *
from django.contrib.auth.models import User
from rest_framework import serializers, viewsets


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    username = request.data.get('email')
    password = request.data.get('password')

    if username is None:
        return Response('Please provide user id and password', status=status.HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if not user:
        return Response('Invalid Credentials', status=status.HTTP_404_NOT_FOUND)

    token, created = Token.objects.get_or_create(user=user)

    context = {
        'id': user.id,
        'token': token.key,
        'email': user.username,
        'name': user.name,
    }

    return Response(context, status=status.HTTP_200_OK)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Personnel
        fields = ['id', 'name', 'username', 'password', 'email', 'tc']


class VideoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Video
        fields = ['id', 'title', 'noq']


class UserViewSet(viewsets.ModelViewSet):
    queryset = Personnel.objects.all()
    serializer_class = UserSerializer


# ViewSets define the view behavior.
class VideoViewSet(viewsets.ModelViewSet):
    queryset = Video.objects.all()
    serializer_class = VideoSerializer
