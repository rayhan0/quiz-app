from django.contrib import admin
from .models import *
# Register your models here.


class PersonnelAdmin(admin.ModelAdmin):
    list_display = ('username', 'name', 'email', 'tc')
    fieldsets = (
        ('Personal info', {'fields': ('name', 'password', 'username','email', 'tc')}),
        ('Permissions', {'fields': ('is_staff', 'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('date_joined', 'last_login', )}),
    )
    list_filter = ('is_superuser', 'groups__name')
    search_fields = ('username',)

    def save_model(self, request, obj, form, change):
        if change:
            obj.last_modified_by = request.user
        else:
            obj.is_staff = True
            obj.created_by = request.user
            obj.last_modified_by = request.user

        if 'password' in form.changed_data:
            obj.set_password(obj.password)
            obj.save()
        else:
            pass

        super(PersonnelAdmin, self).save_model(request, obj, form, change)


admin.site.register(Personnel, PersonnelAdmin)
admin.site.register(Video)
admin.site.register(Question)
admin.site.register(Option)
