from django.db import models
from django.contrib.auth.models import AbstractUser


class Personnel(AbstractUser):
    name = models.CharField(db_column='name', max_length=64)
    password = models.CharField(db_column='password', max_length=256)
    email = models.EmailField(db_column='email', max_length=256)
    tc = models.BooleanField(default=False, db_column='tc')


class Video(models.Model):
    title = models.CharField(max_length=300, unique=True)
    noq = models.IntegerField()



class Question(models.Model):
    title = models.CharField(max_length=300)
    video = models.ForeignKey(Video, related_name='questions', on_delete=models.CASCADE, blank=True, null=True)
    

class Option(models.Model):
    title = models.CharField(max_length=300)
    correct = models.BooleanField(default=False, db_column='correct',)
    option = models.ForeignKey(Question, related_name='options', on_delete=models.CASCADE, blank=True, null=True)
