import React from 'react';
import Illustration from '../Illustration';
import SignInForm from '../SignInForm'


export default function SignIn(){
    return (
        <>
            <h1>Login to your account</h1>
            <div className="column">
            <Illustration/>
            <SignInForm/>
            
            </div>
        </>
    )
}
