import React from 'react';
import Analysis from '../Analysis';
import Summary from '../Summary';


export default function Results(){
    return (
        <>
            <Summary/>
            <Analysis/>
        </>
    )
}
