import React from 'react';
import { useAuth } from "../contexts/AuthContext";
import classes from '../styles/Account.module.css';
import {Link} from 'react-router-dom';

export default function Account(){
    const { currentUser, logout } = useAuth();
    return (
        <div className={classes.account}>
            {currentUser ? (
            <>
                <span className="material-icons-outlined" title="Account">
                account_circle
                </span>
                <span>{currentUser.name}</span>
                <span
                className="material-icons-outlined"
                title="Logout"
                onClick={logout}
                >
                {" "}
                logout{" "}
                </span>
            </>
            ) : (
            <>
                <Link to="/signin">Login</Link>
            </>
            )}
      </div>
    )
}

