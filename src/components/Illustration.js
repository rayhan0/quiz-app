import React from 'react';
import classes from '../styles/Illustration.module.css';
import Image from '../assets/images/signup.svg';

export default function Illustration(){
    return (
        <div className={classes.illustration}>
        <img src={Image} alt="Signup" />
    </div>
    )
}
