import React from 'react';
import classes from '../styles/Layout.module.css';
import Nav from './Nav';

export default function Layout({children}){
    return (
        <>
        <Nav/>
        <main className={classes.name}>
            <div className={classes.container}>
                {children}
            </div>
        </main>
        </>
    )
}
