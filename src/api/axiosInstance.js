import axios from 'axios';

const baseURL = 'http://127.0.0.1:8000/api';
const token = localStorage.getItem('token');

const axiosInstance = axios.create({
  baseURL: baseURL,
  timeout: 5000,
});

axiosInstance.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('token');
    if (token) {
      config.headers.authorization = `Token ${token}`;
    }
    return config;
  },
  (error) => Promise.reject(error),
);

export default axiosInstance;