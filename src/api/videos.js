import axiosInstance from "./axiosInstance";

export const getVideos = (page) =>{
    return axiosInstance
    .get(`/videos/?page=${page}`)
    .then(resp=>{
        console.log(resp);
        return resp.data;
    })
    .catch(err=>(
        err.response
    ))
}