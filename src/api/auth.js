import axiosInstance from "./axiosInstance";

export const signin = (data) =>{
    return axiosInstance
    .post('/login/', data)
    .then(resp=>{
        console.log(resp);
        return resp;
    })
    .catch(err=>(
        err.response
    ))
}

export const registration = (data) =>{
    return axiosInstance
    .post('/users/', data)
    .then(resp=>{
        console.log(resp);
        return resp;
    })
    .catch(err=>(
        err.response
    ))
}