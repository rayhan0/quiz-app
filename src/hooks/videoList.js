  import  { getVideos } from "../api/videos"
  import { useEffect, useState } from "react";
  
  export default function useVideoList(page) {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [videos, setVideos] = useState([]);
    const [hasMore, setHasMore] = useState(true);
  
    useEffect(() => {
      async function fetchVideos() {
        // database related works
        setError(true);
        const video_list = getVideos(page);
        setError(false);
        try {
          setError(false);
          if (video_list.exists()) {
            setVideos((prevVideos) => {
              return [...prevVideos, ...video_list.results];
            });
          } else {
            setHasMore(false);
          }
        } catch (err) {
          console.log(err);
          setLoading(false);
          setError(true);
        }
      }
  
      fetchVideos();
    }, [page]);
  
    return {
      loading,
      error,
      videos,
      hasMore,
    };
  }
  