  import React, { useContext, useEffect, useState } from "react";
  import  { registration, signin } from "../api/auth"
  
  const AuthContext = React.createContext();
  
  export function useAuth() {
    return useContext(AuthContext);
  }
  
  export const AuthProvider = props => {
    const [authLogin, setAuthLogin] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [currentUser, setCurrentUser] = useState();
  
    useEffect(() => {
      const token = localStorage.getItem('token');
      if(token){
        setAuthLogin(true);
        setIsLoading(false);
      }else{
        setIsLoading(false);
      }
    }, []);
  
    // signup function
    async function signup(email, password, name) {
      registration({'email': email, 'password': password, 'username': email, 'name': name})
      .then(resp=>{
          console.log(resp);
          if(resp){
            if(resp.status === 200 || resp.status === 201){
              setAuthLogin(true);
              // user = resp.data
              setCurrentUser({
                ...resp.data,
              });
            }
          }

         
      })
  
      // // update profile
      // await updateProfile(auth.currentUser, {
      //   displayName: username,
      // });
  
      // const user = auth.currentUser;
      // setCurrentUser({
      //   ...user,
      // });
    }
  
    // login function
    function login(email, password) {
      signin({'email': email, 'password': password})
      .then(resp=>{
          console.log(resp);
          if(resp){
            if(resp.status === 200 || resp.status === 201){
              setAuthLogin(true);
              localStorage.setItem('token', resp.data.token)
              setCurrentUser({
                ...resp.data,
              });
            }
          }
      })
    }
  
    // logout function
    // function logout() {
    //   const auth = getAuth();
    //   return signOut(auth);
    // }
  
    const value = {
      currentUser,
      authLogin,
      signup,
      login,
      setAuthLogin,
      isLoading,
      setIsLoading,
    };
  
    return (
      <AuthContext.Provider value={value}>
      {props.children}
    </AuthContext.Provider>
    );
  }
  